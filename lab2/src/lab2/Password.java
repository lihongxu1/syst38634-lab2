package lab2;
/**
 * @author james li991549460
 */


public class Password {

	public static boolean isValidLength (String passIn) {
		String pass  = passIn.trim();
		if ( pass.length() >7 && countDigit(pass) >1 && checkCase(pass)) {
			return true;
		}
		
		else		
		return false;
	}
	
	public static int countDigit (String pass) {
		 int count = 0;
	      char[] chars = pass.toCharArray();
	     
	      for(char c : chars){
	         if(Character.isDigit(c)){
	           count++;
	         }
	      }
	      return count;
	         
	}
	
	public static boolean checkCase (String pass) {
		boolean hasUppercase = !pass.equals(pass.toLowerCase());
		boolean hasLowercase = !pass.equals(pass.toUpperCase());
		
		if (hasUppercase && hasLowercase) {
			return true;
		}
		else return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
