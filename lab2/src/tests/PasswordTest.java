package tests;
/**
 * @author james li991549460
 */
import static org.junit.Assert.*;

import org.junit.Test;

import lab2.Password;

public class PasswordTest {

	/*@Test
	public void testIsValidLengthReg() {
		boolean passCheck = Password.isValidLength("aFGHbcd1234");
		assertTrue("Invalid password length", passCheck);
	}
	
	@Test
	public void testIsValidLengthEx() {
		boolean passCheck = Password.isValidLength("abcdgd     g4");
		assertFalse("Invalid password length", passCheck);
	}	
	
	@Test
	public void testIsValidLengthBIn() {
		boolean passCheck = Password.isValidLength("abcd1234");
		assertTrue("Invalid password length", passCheck);
	}
	
	@Test
	public void testIsValidLengthDBOut() {
		boolean passCheck = Password.isValidLength("abcdrr4");
		assertFalse("Invalid password length", passCheck);
	}*/
	
	@Test
	public void testIsValidDigitReg() {
		boolean passCheck = Password.isValidLength("aFGHbcd1234");
		assertTrue("Invalid password length", passCheck);
	}
	
	@Test
	public void testIsValidDigitEx() {
		boolean passCheck = Password.isValidLength("abcdgdsdewsfeg");
		assertFalse("Invalid password length", passCheck);
	}	
	
	@Test
	public void testIsValidDigithBIn() {
		boolean passCheck = Password.isValidLength("abCd1rer4");
		assertTrue("Invalid password length", passCheck);
	}
	
	@Test
	public void testIsValidDigitBOut() {
		boolean passCheck = Password.isValidLength("abDDDDDDDDD4");
		assertFalse("Invalid password length", passCheck);
	}

}
